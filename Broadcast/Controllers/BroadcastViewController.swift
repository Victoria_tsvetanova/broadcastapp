//
//  UViewController.swift
//  Broadcast
//
//  Created by victoria.tsvetanova on 25.06.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit
import ReplayKit

class BroadcastViewController: UIViewController, AgoraRtcEngineDelegate {
    
    //Maek: - Constants
    let appId = "08c6d6175c1d47eb803047f601b111b2"
    
    //MARK: - Video Views
    @IBOutlet var remoteVideoView: UIView!
    @IBOutlet var localVideoView: UIView!
    
    //MARK: - Buttons
    @IBOutlet var startVideoButton: UIButton!
    @IBOutlet var muteButton: UIButton!
    
    @IBOutlet var broadcastingRedLabel: UILabel!
    @IBOutlet var channelNameLabel: UILabel!
    
    var remoteUserId: UInt? // will be assigned to the first remote broadcaster
    var channelName: String?
    var agoraKit: AgoraRtcEngineKit?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        broadcastingRedLabel.isHidden = true
        
        channelNameLabel.text = channelName ?? "Channel"
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        leaveChannel()
        
    }
    
    //Button actions
    @IBAction func didPressStartButton(_ sender: Any) {
        
        initializeAgoraEngine()
        
        setChannelProfile()
        joinChannelByToken()
        //setClientRole()
        
    }
    
    @IBAction func didPressMuteButton(_ sender: Any) {
        
        if muteButton.titleLabel?.text == "Mute" {
            
            muteButton.setTitle("Unmute", for: .normal)
            agoraKit?.muteAllRemoteAudioStreams(true)
            
        } else {
            
            muteButton.setTitle("Mute", for: .normal)
            agoraKit?.muteAllRemoteAudioStreams(false)
        }
        
    }
    
    @IBAction func didPressStopButton(_ sender: Any) {
        
        broadcastingRedLabel.isHidden = true
        
        leaveChannel()
        
    }
    
    @IBAction func didPressStartVideoButton(_ sender: Any) {
        
        if startVideoButton.titleLabel?.text == "Start Video" {
            
            startVideoButton.setTitle("Stop Video", for: .normal)
            
            enableVideo()
            setVideoEncoderConfiguration()
            setupLocalVideo()
            
            if let remoteUserId = remoteUserId {
                setupRemoteVideo(id: remoteUserId)
            }
            
        } else {
            
            startVideoButton.setTitle("Start Video", for: .normal)
            agoraKit?.disableVideo()
            
        }
        
    }

    //MARK: - Agora methods
    func initializeAgoraEngine() {
        
        agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: appId, delegate: self)
        
    }
    
    func enableVideo() {
        
        agoraKit?.enableVideo() // Default mode is disableVideo.
        
    }
    
    func setChannelProfile() {
        
        agoraKit?.setChannelProfile(.game)
        
    }
    
    func joinChannelByToken() {

        if let channelName = channelName {
            
            agoraKit?.joinChannel(byToken: nil, channelId: channelName, info: nil, uid: 0, joinSuccess: { (sid, uid, elapsed) -> Void in
                
                print("Channel name: \(sid)")
                print("User Id: \(uid)")
                self.broadcastingRedLabel.isHidden = false
                
            })
            
        }
        
    }
    
    func setClientRole() {
        
        agoraKit?.setClientRole(.broadcaster)
        
    }
    
    func leaveChannel() {
        
        agoraKit?.leaveChannel(nil)
        
    }
    
    func setVideoEncoderConfiguration() {
        
        let configuration = AgoraVideoEncoderConfiguration(size:
            AgoraVideoDimension640x360, frameRate: .fps15, bitrate: 400,
                                        orientationMode: .fixedPortrait)
        agoraKit?.setVideoEncoderConfiguration(configuration)
        
    }
    
    func setupLocalVideo() {
        
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = 0
        videoCanvas.view = localVideoView
        videoCanvas.renderMode = .hidden
        agoraKit?.setupLocalVideo(videoCanvas)
        
    }
    
    func setupRemoteVideo(id: UInt) {
        
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = id
        videoCanvas.view = remoteVideoView
        videoCanvas.renderMode = .fit
        agoraKit?.setupRemoteVideo(videoCanvas)
        
    }
}

//MARK: - Agora rtc methods
extension BroadcastViewController {
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        
        print("\(uid) joined ")
        
        remoteUserId = uid
        
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, activeSpeaker speakerUid: UInt) {
        print("Speaker \(speakerUid) is active")
    }
    
    func rtcEngineCameraDidReady(_ engine: AgoraRtcEngineKit) {
        print("Camera is ready")
    }
    
    func rtcEngineMediaEngineDidLoaded(_ engine: AgoraRtcEngineKit) {
        print("Media engine did loaded")
    }
    
    func rtcEngineRequestToken(_ engine: AgoraRtcEngineKit) {
        print("Engine requests token")
    }
    
    func rtcEngineConnectionDidLost(_ engine: AgoraRtcEngineKit) {
        print("Engine connection did lost")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("Engine error occured wtih code \(errorCode.rawValue)")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
        print("Engine did join channel")
    }
    
    func rtcEngineRemoteAudioMixingDidStart(_ engine: AgoraRtcEngineKit) {
        print("Remote audio mixing did start")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didMicrophoneEnabled enabled: Bool) {
        print("Is microphone enabled: \(enabled)")
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteAudioStats stats: AgoraRtcRemoteAudioStats) {
        print(stats)
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteVideoStats stats: AgoraRtcRemoteVideoStats) {
        print(stats)
    }
    
}

