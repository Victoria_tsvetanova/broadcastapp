//
//  StartingViewController.swift
//  Broadcast
//
//  Created by victoria.tsvetanova on 25.06.19.
//  Copyright © 2019 victoria.tsvetanova. All rights reserved.
//

import UIKit

class StartingViewController: UIViewController {
    
    private var broadcastViewController: BroadcastViewController?
    private var channelName: String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        makeNavigationBarTransparent()

    }
    
    func makeNavigationBarTransparent() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let broadcastViewController = segue.destination as? BroadcastViewController
        broadcastViewController?.channelName = channelName

    }
    
    @IBAction func didPressChannel1(_ sender: Any) {
        
        channelName = "Channel 1"
        performSegue(withIdentifier: "goToChannel", sender: self)
        
    }
    
    @IBAction func didPressChannel2(_ sender: Any) {
        
        channelName = "Channel 2"
        performSegue(withIdentifier: "goToChannel", sender: self)
        
    }
    
    

}
